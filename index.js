"use strict";

let count = 0;
let timer;


//создаю поле 10*10
class Field{
   createField(){
    let table = document.createElement("table");
    table.id = "table";
    let tr;
    
    for(let i = 1; i <= 10; i++){
     tr = document.createElement("tr");
     tr.classList.add("rows");
     let th;
        for(let i = 1; i <= 10; i++){
         th = document.createElement("th");
         th.classList.add("cell");
           tr.append(th);
        }
        
           table.append(tr);
    }
    return table;
   }


getBlueColor(){
    let cells = document.querySelectorAll(".cell");
    const random = Math.floor(Math.random() * cells.length);
    
    cells.forEach(( cell, index ) => {    // делаю синий цвет
        cell.id = index;
        if(random === Number(cell.id)){
            cell.classList.add("blue-cell");
        } else {
            cell.classList.remove("blue-cell");
        }    
    });      
}

getGreenRedColor(){                                   // делаю зеленый и красный цвет
    let table = document.getElementById("table");

    table.addEventListener("click", (e) => {
    let target = e.target;
    
            if(target.tagName !== "TH") {
                return false;
            } else {
                if(target.classList.contains("blue-cell")){
                    target.classList.add("green-cell");
                    let player1 = document.getElementById("playerOne");
                    player1.textContent = green();
                } else {
                    target.classList.add("red-cell");
                    let player2 = document.getElementById("playerTwo");
                    player2.textContent = red();
                } 
            } 
            scoreBoard.countScore(); // запускаю метод подсчета очков
        })
    }
}


// делаю табло с очками 
class WindowCount{

    getTablo(){
        let div = document.createElement("div");
        div.classList = "score-board";


        let p1 = document.createElement("p");
        p1.textContent = "Вы";

        let span1 = document.createElement("span");
        span1.id = "playerOne";
        span1.textContent = "0";
        p1.after(span1);
       

        let p2 = document.createElement("p");
        p2.textContent = "Компьютер";
        let span2 = document.createElement("span");
        span2.id = "playerTwo";
        span2.textContent = "0";

        p2.after(span2);
        
        div.append(p1, span1, p2, span2);
        return div;
    }

    //считаю очки
    countScore(){

        count++;
        console.log(count);
        
        if(count === 51){
           let redsell = document.querySelectorAll(".red-cell");
           let greencell = document.querySelectorAll(".green-cell");
           let result = "";
    
           if(redsell.length > greencell.length) {
            
            result = `Счёт ${greencell.length} : ${redsell.length}. Победил компьютер!`
           } else if(redsell.length < greencell.length) {
            result = `Счёт ${greencell.length} : ${redsell.length}. Вы победили!`
           } else {
            result = `Счёт ${greencell.length} : ${redsell.length}. Ничья!`
           }
        
        let div = document.createElement("div");
        let text = document.createElement("p");
        text.id = "result";
        text.textContent = `Игра окончена! ${result}`;
        div.classList.add = "result";
        div.append(text);
        document.body.append(div);
        clearInterval(timer);
        }
        }

}

// счетчик для очков игроков
function counter(){
    let count1 = 0;
    return function(){
       return ++count1;
    }
}



let green = counter();
let red = counter();



//кнопки выбора уровня сложности, старт, стоп, начать заново
class Buttons{
   
render(){
    
const wrap = document.createElement("div");
const h5 = document.createElement("h5");
h5.textContent = "Выберите уровень сложности"
const form = document.createElement("form");
form.innerHTML = `
<select id="level" class="form-select form-select-sm" aria-label=".form-select-sm example">
  <option value="1">Легкий</option>
  <option value="2">Средний</option>
  <option value="3">Сложный</option>
</select>`



let stop = document.createElement("button");
let start = document.createElement("button");
let startOver = document.createElement("button");

start.textContent = "START";
start.classList = "btn btn-success"
stop.textContent = "STOP";
stop.classList = "btn btn-warning";
startOver.textContent = "Играть заново";
startOver.classList = "btn btn-danger";
wrap.prepend(h5, form, start, stop, startOver);

stop.addEventListener("click", () => {
    clearInterval(timer);
})


start.addEventListener("click", () => {
    selectLevel();  
})

startOver.addEventListener("click", ()=> {
    let cells = document.querySelectorAll(".cell");
    const arrayOfCells = Array.from(cells);
    let player1 = document.getElementById("playerOne");
    let player2 = document.getElementById("playerTwo");
    let question = confirm("Вы точно хотите начать игру заново?");
    if(question){
        arrayOfCells.forEach(item => {
            if(item.classList.contains("red-cell") || 
               item.classList.contains("green-cell") || 
               item.classList.contains("blue-cell")){
                item.classList.remove("red-cell");
                item.classList.remove("green-cell");
                item.classList.remove("blue-cell");
            }
        })
        clearInterval(timer);
        console.log("хорошо, скоро начнем");

        let text = document.getElementById("result");  // убираю финальную строку с результатом, если новый раунд 
        if(text !== null){
            text.textContent = "";
        }
       
        
        player1.textContent = "0";
        player2.textContent = "0";
        count = 0;
        green = counter();
        red = counter();
        selectLevel();
        
    } else {
        alert("До встречи!");
        arrayOfCells.forEach(item => {
            if(item.classList.contains("red-cell") || 
               item.classList.contains("green-cell") || 
               item.classList.contains("blue-cell")){
                item.classList.remove("red-cell");
                item.classList.remove("green-cell");
                item.classList.remove("blue-cell");
            }
        player1.textContent = "0";
        player2.textContent = "0";
        count = 0;
        green = counter();
        red = counter();
        clearInterval(timer);
        })
       
    }
})

document.body.append(wrap);
return wrap;
    }
}

let field = new Field();
let scoreBoard = new WindowCount();
const btns = new Buttons();



document.body.prepend(btns.render(), field.createField(), scoreBoard.getTablo());


field.getGreenRedColor(green, red);


//функция для выбора уровня сложности игры
function selectLevel(){
    let e = document.getElementById("level");
    let levelValue = e.options[e.selectedIndex].value;
    if(levelValue === "1"){
        timer = setInterval(() => { field.getBlueColor() }, 1500);
    } else if(levelValue === "2"){
       timer = setInterval(() => { field.getBlueColor() }, 1000);
    } else {
        timer = setInterval(() => { field.getBlueColor() }, 500);
    } 
}











